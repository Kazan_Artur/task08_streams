package com.kazan.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Main {
    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        ListGenerator listGenerator = new ListGenerator();
        List<Integer> listFromArray = listGenerator.makeStreamFromArray();
        List<Integer> listFromStreamOf = listGenerator.makeStreamOf();
        List<Integer> listFromStreamBuilder = listGenerator.useStreamBuilder();
        List<Integer> listFromStreamIterator = listGenerator.useStreamIterator();

        double avg = countAverage(listFromArray);
        log.info("avg= " + avg);

        int min = min(listFromStreamOf);
        log.info("min= " + min);

        int max = max(listFromStreamBuilder);
        log.info("max= " + max);

        double sum = sum(listFromStreamIterator);
        log.info("sum= " + sum);

        int sumReduce = sumReduce(listFromStreamIterator);
        log.info("sumReduce= " + sumReduce);

        long l = countBiggerThanAverage(listFromArray);
        log.info("number of values that are bigger than average= "+l);
    }


    private static double countAverage(List<Integer> list) {
        log.info("Count average");
        return list.stream()
                .mapToInt(Integer::intValue)
                .average().
                        getAsDouble();
    }

    private static int min(List<Integer> list) {
        log.info("Count min");
        return list.stream()
                .mapToInt(Integer::intValue)
                .min()
                .getAsInt();

    }

    private static int max(List<Integer> list) {
        log.info("Count max");
        return list.stream()
                .mapToInt(Integer::intValue)
                .max()
                .getAsInt();

    }

    private static double sum(List<Integer> list) {
        log.info("Count sum");
        return list.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    private static int sumReduce(List<Integer> list) {
        log.info("Count sum with reduce");
        return list.stream()
                .mapToInt(Integer::intValue)
                .reduce((a, b) -> a + b).getAsInt();
    }

    private static long countBiggerThanAverage(List<Integer> list) {
        log.info("Count number of values that are bigger than average");
        double avg = countAverage(list);
        return list.stream()
                .filter(n -> n > avg)
                .count();
    }
}
