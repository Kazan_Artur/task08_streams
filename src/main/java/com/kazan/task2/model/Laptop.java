package com.kazan.task2.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Laptop extends Device {

    private static Logger log = LogManager.getLogger(Laptop.class);


    @Override
    public void turnOn() {
        log.info("Laptop is turnOn");
    }

    @Override
    public String turnOff() {
        log.info("Laptop is turnOff");
        return "Laptop turnOff";
    }

    @Override
    public void restart() {
        log.info("Laptop restarted");
    }

    @Override
    public void lock() {
        log.info("Laptop is locked");
    }

    @Override
    public String toString() {
        return "Laptop{}";
    }
}
