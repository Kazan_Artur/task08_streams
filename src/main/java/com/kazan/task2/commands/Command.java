package com.kazan.task2.commands;

public interface Command {

    void execute();
}
