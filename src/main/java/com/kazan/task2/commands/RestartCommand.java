package com.kazan.task2.commands;

import com.kazan.task2.model.Device;

public class RestartCommand implements Command {

    private Device device;

    public RestartCommand(Device device) {
        this.device = device;
    }

    @Override
    public void execute() {
        device.restart();
    }

    @Override
    public String toString() {
        return "RestartCommand{" +
                "device=" + device +
                '}';
    }
}
