package com.kazan.task2;

import com.kazan.task2.commands.Command;
import com.kazan.task2.model.Device;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class User {
    private static Logger log = LogManager.getLogger(User.class);
    public final List<Command> operations = new ArrayList<>();



    public void executeOperation(Command command) {
        operations.add(command);
        log.info("Execute operation: " + command.toString());
    }

    public void lock(Device device){
        device.lock();
    }

    public void turnOff(Device device){
        device.turnOff();
    }
}
