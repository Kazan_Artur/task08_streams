package com.kazan.task1;

public interface Tester {
    int compute (int a, int b, int c );
}
