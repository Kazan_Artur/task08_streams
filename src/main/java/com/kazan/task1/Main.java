package com.kazan.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        Tester max = (a, b, c) -> Integer.max(Integer.max(a, b), c);
        int x = max.compute(45, 12, 27);
        log.info("max= " + x);

        Tester average = (a, b, c) -> (a + b + c) / 3;
        int a = average.compute(8, 16, 32);
        log.info("average= " + a);
    }

}
